// Challenge 02: Icon and Title

import SwiftUI
import PlaygroundSupport

struct ContentView: View {
    @State var username = ""
    @State var password = ""
    
    var body: some View {
        
        HStack {
            
            Image(uiImage: UIImage(named: "Icon.png")!)
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100, alignment: .center)
                .cornerRadius(24)
            
            
            VStack(alignment: .leading, spacing: 0) {
                Label("RoadCam", systemImage: "")
                    .font(.title)
                    .foregroundColor(.white)
                Label("Roland Heusser", systemImage: "")
                    .font(.subheadline)
                    .foregroundColor(.gray)
                Spacer()
                HStack {
                    Button("OPEN", action: {})
                        .font(.system(size: 12, weight: .bold, design: .default))
                        .frame(width: 58, height: 22, alignment: .center)
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .cornerRadius(10)
                    Spacer()
                    Image(systemName: "square.and.arrow.up")
                        .foregroundColor(.blue)
                }
            }
            
        }
        .padding()
        .background(Color.black)
        .padding()
    }
}

let view = ContentView()
PlaygroundPage.current.setLiveView(view)


