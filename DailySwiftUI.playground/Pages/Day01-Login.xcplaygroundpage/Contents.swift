// Challenge 01: Simple Login

import SwiftUI
import PlaygroundSupport

struct ContentView: View {
    @State var username = ""
    @State var password = ""
    
    var body: some View {
        
        VStack {
            VStack(spacing: 16.0) {
                Text("Welcome")
                VStack {
                    TextField("Username field", text: $username, prompt: Text("Username..."))
                    SecureField("Password field", text: $password, prompt: Text("Password.."))
                }
                .frame(width: 150)
                Button("Login", action: {})
            }
            .frame(width: 200, height: 180, alignment: .center)
            .padding()
            .background(Color.white)
            
            .cornerRadius(5.0)
            .shadow(color: .gray, radius: 5.0, x: 1.0, y: 1.0)
        }
        .padding()
    }
}

let view = ContentView()
PlaygroundPage.current.setLiveView(view)
